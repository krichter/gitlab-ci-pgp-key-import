#!/bin/sh -e
echo "$PGP_KEY" > pgp.asc
diff pgp.asc codesigning.asc
#mkdir /root/.gnupg
#echo "\nuse-agent\npinentry-mode loopback" >> /root/.gnupg/gpg.conf
#echo "\nallow-loopback-pinentry" >> /root/.gnupg/gpg-agent.conf
gpg --batch --fast-import pgp.asc
